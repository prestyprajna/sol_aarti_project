﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductSupply.aspx.cs" Inherits="Sol_Aarti.ProductSupply" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style type="text/css">
        table{
            margin:auto;
            padding:100px;          

        }

       .textBoxStyle{
           width:400px;
           height:50px;
           border-radius:10px;
       }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtRetailerName" runat="server" placeholder="Retailer Name" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtQuantity" runat="server" placeholder="Quantity" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>            
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlOption" runat="server" CssClass="textBoxStyle w3-white">
                        <asp:ListItem Value="0" Text="--- SELECT OPTION ---"></asp:ListItem>
                        <asp:ListItem Value="1" Text="OPTION1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="OPTION2"></asp:ListItem>
                        <asp:ListItem Value="3" Text="OPTION3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                     <asp:TextBox ID="txtEmailId" runat="server" placeholder="EmailId" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ImageButton ID="imgPdfInvoice" runat="server" AlternateText="PDF INVOICE"  CssClass="w3-button w3-blue" />                      
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="w3-button w3-red" />
                </td>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="w3-button w3-red" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
