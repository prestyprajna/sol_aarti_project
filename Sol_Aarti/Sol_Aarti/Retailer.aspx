﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Retailer.aspx.cs" Inherits="Sol_Aarti.Retailer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

    <style type="text/css">
        table{
            margin:auto;
            padding:100px;          

        }

       .textBoxStyle{
           width:400px;
           height:50px;
           border-radius:10px;
       }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtRetailerName" runat="server" placeholder="Retailer Name" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtRetailerAddress" runat="server" placeholder="Retailer Address" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr> 
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtRetailerLocation" runat="server" placeholder="Retailer Location" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>      
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtRetailerPincode" runat="server" placeholder="Retailer Pincode" CssClass="textBoxStyle w3-white" TextMode="Number"></asp:TextBox>   
                </td>
            </tr>  
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtMobileNo" runat="server" placeholder="Mobile Number" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>     
            <tr>
                <td colspan="3">
                     <asp:TextBox ID="txtEmailId" runat="server" placeholder="EmailId" CssClass="textBoxStyle w3-white"></asp:TextBox>   
                </td>
            </tr>                
            <tr>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="w3-button w3-blue" />
                </td>
                <td>
                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="w3-button w3-blue" />
                </td>
                <td>
                    <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="w3-button w3-blue" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
